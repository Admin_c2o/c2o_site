<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "contact_data".
 *
 * @property int $id
 * @property string $name
 * @property string $value
 * @property string $description
 * @property string $comment
 */
class Contactdata extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'contact_data';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id', 'name', 'value', 'description', 'comment'], 'required'],
            [['id'], 'integer'],
            [['comment'], 'string'],
            [['name', 'value', 'description'], 'string', 'max' => 255],
            [['name'], 'unique'],
            [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'value' => 'Value',
            'description' => 'Description',
            'comment' => 'Comment',
        ];
    }
}
