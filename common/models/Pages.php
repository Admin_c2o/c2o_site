<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "pages".
 *
 * @property int $id ID
 * @property string $name Название страницы
 * @property string $templateRU Шаблон русский
 * @property string $templateEN Шаблон английский
 * @property string $chpu ЧПУ термин
 * @property string $titleRU Title Рус
 * @property string $titleEN Title En
 * @property string $descRU Description РУсский
 * @property string $descEN Description Английский
 * @property string $created_at Дата создания
 */
class Pages extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'pages';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'template', 'chpu', 'created_at'], 'required'],
            [['created_at'], 'safe'],
            [['name', 'template', 'chpu'], 'string', 'max' => 255],
            [['chpu'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название',
            'template' => 'Шаблон',
            'chpu' => 'ЧПУ',
            'created_at' => 'Дата создания',
        ];
    }
}
