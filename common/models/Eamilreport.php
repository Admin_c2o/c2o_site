<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "eamil_report".
 *
 * @property int $id_report
 * @property string $type
 * @property string $link
 * @property string $domain
 * @property string $phone
 * @property string $summa
 * @property string $schet
 * @property string $message
 * @property string $date_created
 */
class Eamilreport extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'eamil_report';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['message'], 'string'],
            [['date_created'], 'required'],
            [['date_created'], 'safe'],
            [['type', 'link', 'domain', 'phone', 'summa', 'schet'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_report' => 'Id Report',
            'type' => 'Type',
            'link' => 'Link',
            'domain' => 'Domain',
            'phone' => 'Phone',
            'summa' => 'Summa',
            'schet' => 'Schet',
            'message' => 'Message',
            'date_created' => 'Date Created',
        ];
    }
}
