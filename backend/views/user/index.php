<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\UserSearchModel */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('app', 'Пользователи');



$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php Pjax::begin(); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a(Yii::t('app', 'Регистрация нового пользователя'), ['signup'], ['class' => 'btn btn-success']) ?>
    </p>
	
	

	

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            //'id',
            'username',
            //'auth_key',
            //'password_hash',
            //'password_reset_token',
            'email:email',
            //'status',
            //'created_at',
            //'updated_at',
					
			
			[
				'attribute' => 'type',
				'value' => function($data) {
										
							
							$str ='';
							
							
							
							$str.= $data['auth_assignment'][0]['item_name'];
							
							
							return $str;
							
							
                },
				
				
				
			],
			
			
			

            [
				'class' => 'yii\grid\ActionColumn',
				'template' => '{update}   {delete} ',
				'visibleButtons' => [
									'update' => function ($model) {
										return \Yii::$app->user->can('admin', ['post' => $model]);
									},
									'delete' => function ($model) {
										return \Yii::$app->user->can('admin', ['post' => $model]);
									},
								],
			
			
			],
        ],
    ]); ?>
    <?php Pjax::end(); ?>
</div>
