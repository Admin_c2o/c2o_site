<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model common\models\Contactdata */

$this->title = 'Create Contactdata';
$this->params['breadcrumbs'][] = ['label' => 'Contactdatas', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="contactdata-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
