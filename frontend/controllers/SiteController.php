<?php
namespace frontend\controllers;

use Yii;
use yii\base\InvalidParamException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use common\models\Eamilreport;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;

use app\models\Filesdata;

use app\models\Files;

use common\models\Pages;

/**
 * Site controller
 */
class SiteController extends Controller
{




	

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
		
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
					
					
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
			
			
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }




	public function beforeAction($action)
	{      
		parent::beforeAction($action);


	
		/*if ($this->action->id == 'new') {
			$this->enableCsrfValidation = false;
		}*/
		$countries_arr = ['AB', 'BY', 'GE', 'KZ', 'KG', 'MD', 'RU', 'TJ', 'TM', 'UZ', 'UA', 'OS'];
			
		
		$cookies = Yii::$app->response->cookies;

		if (empty($cookies['language'])) {
						
			// добавление новой куки в HTTP-ответ
			$cookies->add(new \yii\web\Cookie([
				'name' => 'language',
				'value' => 'RU',
			]));
			
		} 



		

		if (!empty(Yii::$app->request->cookies['language']) && Yii::$app->request->cookies['language']->value == 'EN') {
			// добавление новой куки в HTTP-ответ
			$cookies->add(new \yii\web\Cookie([
				'name' => 'language',
				'value' => 'EN',
			]));
			
			
		} else {
			
			// добавление новой куки в HTTP-ответ
			$cookies->add(new \yii\web\Cookie([
				'name' => 'language',
				'value' => 'RU',
			]));
		}




		if (!empty($_GET['lang']) && $_GET['lang'] == 'EN')  {
						
			//echo 'English';
			$cookies = Yii::$app->response->cookies;

			//unset($cookies['language']);


			$cookies->add(new \yii\web\Cookie([
				'name' => 'language',
				'value' => 'EN',
			]));
			
			
						
		} elseif (!empty($_GET['lang']) && $_GET['lang'] == 'RU') {
			
			$cookies = Yii::$app->response->cookies;

			$cookies->add(new \yii\web\Cookie([
				'name' => 'language',
				'value' => 'RU',
			]));
			
			
		} 	


			
			
	
	
		
		//Проверяем, есть ли кука, если нет, то ставим в зависимости от 
		if (empty($cookies['language'])) {

			$lang = 'RU';
			
			$data = Yii::$app->geo->getData($_SERVER['REMOTE_ADDR']);
			
			if (!empty($data['country'] && in_array($data['country'], $countries_arr))) {
				
				$lang = 'RU';
				
			} elseif (!empty($data['country'] && !in_array($data['country'], $countries_arr))) {
				
				$lang = 'EN';
				
			}
			
			
			if ($_SERVER['REMOTE_ADDR'] == '127.0.0.1')  {
				
				//$lang = 'EN';
				$lang = 'RU';		
			}
			
			
					
			// добавление новой куки в HTTP-ответ
			$cookies->add(new \yii\web\Cookie([
					'name' => 'language',
					'value' => $lang,
				]));
			
			
		}
		
	
		
		return true;
	}



    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
		
		
		$cookies = Yii::$app->response->cookies;
		
	
		// альтернативный способ получения куки "language"
		if (($cookie = $cookies->get('language')) !== null) {
			$language = $cookie->value;
		}
	
		$modelContact = new ContactForm();
	
		
        return $this->render('index',
		[
			'modelContact' => $modelContact,
			'language' => $cookies->get('language'),
		]);
		
		
		
    }
	
	
	
	public function actionRequiziti () {
				
		return $this->render('requiziti',
					[]);
	
	} 
	
	
	public function actionView ($name) {
	
		
		$models = Pages::find()->where(['chpu' => $name])->all();
		
		
		$cookies = Yii::$app->response->cookies;

		// альтернативный способ получения куки "language"
		if (($cookie = $cookies->get('language')) !== null) {
			$language = $cookie->value;
		}

	
		if (!empty($models[0])) {
			
			$model = $models[0];
			
			$template = $model->template . $language;
			
			return $this->render($template,
					[
						'model' => $model,
						'language' => $language,
					]);
			
			
			
		} else {
			
			return $this->render('//site/error', array('message'=> 'Страница не найдена', 'code' => 404));
		}
		


		
	
	} 
	
	
	
	

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return mixed
     */
    public function actionContact()
    {
        $model = new ContactForm();
			
		
		
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
				
            if ($model->sendEmail('info@c2o.me')) {
                Yii::$app->session->setFlash('success', 'Ваша заявка успешно отправлена!');
	
				return $this->redirect(['index', 'flash' => 'success']);
					
            } else {
                Yii::$app->session->setFlash('error', 'Произошли ошибки во время отправки сообщения.');		
				return $this->redirect(['index', 'flash' => 'error']);
			}

			
        } else {
            return $this->render('contact', [
                'model' => $model,
            ]);
        }
    }

	
	
	
	
	 public function actionContactajax()
    {
        $model = new ContactForm();
				
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
					
			$model->sendEmail('info@c2o.me');

			echo 0;
					
            if ($model->sendEmail('info@c2o.me')) {
				echo 0;			
            } else {
                echo 1;
			}
			
        } else {
            echo 1;
        }
    }

	
	
	
	
	
	
    /**
     * Displays about page.
     *
     * @return mixed
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionSignup()
    {
        $model = new SignupForm();
		
	
		
        if ($model->load(Yii::$app->request->post())) {
			
			
            if ($user = $model->signup()) {
				
				
				$auth_key = $user->auth_key;
				$mail = $user->email;
				
				$link = '<a href="'.$_SERVER['SERVER_NAME'] . '/admin/validateregister?auth_key='.$auth_key.'">Ссылка для активации аккаунта</a>';
				
				mail($mail, 'GZ AUTO AUTH CODE', $link);
				
				return $this->render('signupsuccess', [
					'model' => $model,
				]);
				
            }
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestpasswordreset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');

                return $this->goHome();
            } else {
                Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for the provided email address.');
            }
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
     */
    public function actionResetPassword($token)
    {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidParamException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password saved.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }
	
	
	
	
	
	
}
