<?php

/* @var $this yii\web\View */


$this->title = 'Click to Offer - высокотехнологичная платформа для
	   коммуникации, информирования и сбора задолженностей с ваших клиентов';
?>



<div class="site-index">

<section id="" class="first-banner">


    <div class="container flex-center-align">

    <div class="row">
	<div class="col-md-8" style="text-align: left">			
	   <h1>About C2O</h1>

	   <p class="lead">
	   Personalized payment / repayment proposal delivery through any sources (SMS, e-mail, IVR).
		<br>
		<br>
	  Client / borrower access to payment / debt information without additional authorization steps.
		
		<br>
		<br>
		C2O guarantees an exceptional level of security. Your client, even a non-contact one, receives comprehensive information about the payment / debt and options to choose.

<br>
		<br>
		Positive - will affect the relationship of the lender / supplier, negative - will affect the future of the client.

	   
	   </p>
	   
	</div>
	
	<div class="col-md-4">
	 
		<img src="../images/o_kompanii.png">

    </div>
	
	</div>
	
	</div>
	
</section>	
	
	
	
<!-- ВТорая секция -->	
	
<section id="" class="second-sec">	
<div class="container">	
	
	
	<div class="row">
	<!--
		<div class="col-md-6">
			<img style="width: 75%;" src="images/2.png">
		</div>
	-->
	
		<div class="col-md-9">
			
			
			<p class="blue-text">Solution</p>
			<h2>Analytics</h2>
			
			<p class="lead">
	The analytical module of the C2O platform is a system that analyzes the actions of each client / borrower, or groups of clients, providing analytics after each campaign.<br>
Assessment of the degree of customers' immersion in the service, the time of acquaintance with information, the number of payments. <br>
Tracking internal relationships within the service, as well as recommendations for changing, improving trigger campaigns.

			</p>
			
			
		</div>
		
		
		<div class="col-md-3">
			<img src="../images/o_producte_1.png">
			
				   
	   <br>
	   <br>
	   
	   	
	   
	   
		</div>
		
		
		
		<div class="col-md-3">
			<img src="../images/o_producte_2.png">
			
				   
	   <br>
	   <br>
	   
	   	
	   <div class="clear"></div>
		<br>
	   
		</div>
		
		
		
		
		
		<div class="col-md-9">
			
			
			<p class="blue-text">Solution</p>
			<h2>algorithm intelligence

</h2>
			
			<p class="lead">
			
			Your client receives an SMS message containing an invitation to go to the service, using a unique personal link with OGP preview

			
			
			
			
			</p>
			
			
			<p class="lead">
			Clicking the link, the client enters the service through mobile device. He interacts with debt info to make a decision for payment
			
			</p>
			
			
			<p class="lead">
			You analyze money transfers, detailed C2O report about all client actions and action chains inside the service
			
			</p>
			
			
		</div>
		
		
		
		
		
		
		<div class="clear"></div>
		
		
		
	<div class="col-md-12">
	
	<br>
	<h2>Make a request<span  class="zayavka blue-zayavka">
			Order
		</span></h2> 
	
	
	</div>
	</div>
	
	
	
</div>	
</section>	
	
	
	

	
</div>
