<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

$this->title = 'Contact';
$this->params['breadcrumbs'][] = $this->title;
?>
  

            <?php $form = ActiveForm::begin(
											['id' => 'contact-form', 
											'action' => '/site/contactajax',
											
											'options' => [
												'class' => 'popup-form',
											],
											
											]
											
											); ?>
						

<br><br>						
											
			
			<input type="text" placeholder="Имя" id="contactform-name" class="form-control form-control-new" name="ContactForm[name]" autofocus="" required aria-required="true" style="">

			
			<input type="phone" placeholder="+7(999)999-99-99" id="contactform-name" class="form-control form-control-new" required name="ContactForm[phone]" autofocus="" aria-required="true">
				
			<input type="email" placeholder="your@email.com" id="contactform-name" class="form-control form-control-new" name="ContactForm[email]" required autofocus="" aria-required="true">
				
				

				
                <div class="form-group">
                    <?= Html::submitButton('Отправить', ['class' => 'btn btn-primary newbutton-submit', 'name' => 'contact-button']) ?>
                </div>

            <?php ActiveForm::end(); ?>

	