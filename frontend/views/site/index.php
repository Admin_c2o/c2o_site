<?php

/* @var $this yii\web\View */

use yii\helpers\Url;
use yii\bootstrap\Modal;


if ($language == 'RU') {

	$this->title = 'Click to Offer - высокотехнологичная платформа для коммуникации, информирования и сбора задолженностей с ваших клиентов';

} else {
		
	$this->title = 'Click to Offer is an innovative platform for informing, communication and debt recovery';
	
}


?>



<div class="site-index">




<?php  echo $this->render('first-section'.$language, []); ?>
	
	
	
<!-- ВТорая секция -->	
	

<?php  echo $this->render('second-section'.$language, []); ?>
	
	
	
	
	
<!-- Третья секция -->	
	
<?php  echo $this->render('third-section'.$language, []); ?>	
	
	

<!-- Четвертая секция -->	
	
	
<?php  echo $this->render('fourth-section'.$language, []); ?>	
	
	

	
	
	
	
<!-- Пятая секция -->	


<?php  echo $this->render('fifth-section'.$language, []); ?>	
	
	
<?php  echo $this->render('how-section'.$language, []); ?>		



	
	




	
</div>
