
<?php 

use yii\helpers\Url;

?>




<section id="" class="how-work-sec">
	<div class="container">
	<div class="row">
		<div class="container">
			<h2>How it works</h2>
		</div>
		</div>
	</div>
	
	
	
	<div class="bg-siren">
	
		<div class="container">
	
		<div class="row">
			
				
				<div class="">
				
				
					<div class="col-md-4">
						
						<div class="steps">
						<span class="st-wr">Step 1</span>
						
						<div class="img-wrap-step">
						
						<img style="width: 100%;" src="images/step_1.png">
						</div>	
						
						
						
						
						
						</div>
						
						<div class="step-text">
							
							<p>
							Your client receives an SMS message containing an invitation to go to the service, using a unique personal link with OGP preview
							
							</p>
							
						</div>
						
						
					</div>
					<div class="col-md-4">
					<div class="steps">
						<span class="st-wr">Step 2</span>
						<div class="img-wrap-step">
						<img style="width: 100%;" src="images/step_2.png">
						</div>
						
						
						
					</div>
					
					
					<div class="step-text">
							
							<p>
							Clicking the link, the client enters the service through mobile device. He interacts with debt info to make a decision for payment
						
							</p>
							
						</div>
						
					
					
					</div>
					
					<div class="col-md-4">
					<div class="steps">
						<span class="st-wr">Step 3</span>
						<div class="img-wrap-step wide">
						<img style="width: 100%;" src="images/step_3.png">
						</div>
						
						
						
						
					</div>
					
					<div class="step-text">
							
							<p>
							
							You analyze money transfers, detailed C2O report about all client actions and action chains inside the service


							
							</p>
							
						</div>
					
					
					</div>
				</div>
			
			</div>
			
		</div>		
			
	</div>
	



</section>	











<section class="carousel-sec">
		
	
<div class="container">


<div class="row">

<div class="container">
<h2>Our partners</h2>

</div>
<br>

<?php
use kv4nt\owlcarousel\OwlCarouselWidget;

OwlCarouselWidget::begin([
    'container' => 'div',
    'assetType' => OwlCarouselWidget::ASSET_TYPE_CDN,
    'containerOptions' => [
        'id' => 'container-id',
        'class' => 'container-class owl-theme'
    ],
    'pluginOptions'    => [
        'autoplay'          => true,
        'autoplayTimeout'   => 5000,
        'items'             => 5,
        'loop'              => true,
        'itemsDesktop'      => [1100, 3],
        'itemsDesktopSmall' => [979, 3],
		'nav' => 'true',
    //navText : ["",""],
    ]
]);
?>

<div class="item-class" ><img src="images/mtsabank_new_logo.svg" alt="Image 1"></div>
<div class="item-class"><img src="images/logo_orig.svg" alt="Image 2"></div>
<div class="item-class"><img src="images/a.svg" alt="Image 3"></div>
<div class="item-class"><img src="images/header-logo-slogan.svg" alt="Image 4"></div>
<div class="item-class"><img src="images/pik-komf.svg" alt="Image 5"></div>



<?php OwlCarouselWidget::end(); ?>

</div>	
</div>	
	
	
	
</section>










	
	