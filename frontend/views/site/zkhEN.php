<?php

/* @var $this yii\web\View */


$this->title = 'Click to Offer - высокотехнологичная платформа для
	   коммуникации, информирования и сбора задолженностей с ваших клиентов';
	   
	   
	   
	   
?>



<div class="site-index">

<section id="" class="first-banner">


    <div class="container flex-center-align">

    <div class="jumbotron">
       

	   <h1>Housing and utility enterprises </h1>

	   <p class="lead">
	   Effective distant tools for debt recovery, rent collection 
	   </p>

	   <img style="width: 35%;" class="" src="../images/2.png">

	   </div>

	</div>
	
</section>	
	
	
	
<!-- ВТорая секция -->	
	
<section id="" class="second-sec">	
<div class="container">	
	
	
	<div class="row">

	
		<div class="col-md-8">

			<p class="blue-text">Solution</p>
			<h2>Housing and utility enterprises 
			</h2>
			
			<p class="lead">
			Convenient remote payments, electronic receipts with payment options, debt recovery, renewal tools, convenient information and payment channels (autoinformer, SMS, email in any combination), analytics, trigger campaigns
		</div>
		
		
		<div class="col-md-4 img-box-wr">
			<img  src="../images/JKH_1.png">		   
	   <br>
	   <br>
	   
	   	
	   
	   
		</div>
		
		
		
		
		<div class="col-md-12">
		
			<div class="row">
			
				<div class="col-md-4">
				
						
					<div class="clear"></div>
					<hr>
				
					
					<ul class="main-ul">
						<li>
							Full compliance with local debt recovery and personal data laws
						</li>
					
						<li>
							Debt restructuring tools, rollovers, promised payments
						</li>
					
						
					</ul>
					
					<div class="clear"></div>
				<hr>
				
				
				</div>
				
				<div class="col-md-4">
				
						
					<div class="clear"></div>
					<hr>
				
					<ul class="main-ul">
					
						<li>
							3 click online payments

						
						</li>
						
						<li>
								No password access to the personal account with all info about debt and payment scenarios
						
						</li>
						
						
						
						
					
					</ul>	
					
					<div class="clear"></div>
				<hr>
					
				</div>
				
				
				
				<div class="col-md-4">
				
						
					<div class="clear"></div>
				<hr>
				
					<ul class="main-ul">
					
						<li>
							Multimedia interaction: banners, videos, photos, dynamic documents / pre-trial claims
						</li>
					
					
				
						
						<li>
							IVR + C2O changes the interaction scenario depending on borrower actions 
						
						</li>
						
					
					</ul>	
					
					<div class="clear"></div>
				<hr>
					
				</div>
				
				
			
			</div>
			
			
		</div>
	
	
	<div class="clear"></div>
	
	<br>
	<div class="col-md-12">
	<h2>Call me back and we will contact you shortly <span  class="zayavka blue-zayavka">
			Make a call
		</span></h2> 
	</div>
	
	</div>
	
	
	
</div>	
</section>	
	
	
	

	
</div>
