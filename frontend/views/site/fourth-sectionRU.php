<?php 

use yii\helpers\Url;

?>
	
	
	
	
<section id="" class="second-sec">	
<div class="container">	
	
	
	<div class="row">
	
		<div class="col-md-6">
			<img style="width: 75%;" src="images/4.png">
		</div>
	
	
		<div class="col-md-6">
			
			
			<p class="blue-text">Решение</p>
			<h2>Банковский сектор</h2>
			
			<p class="lead">
		Организация быстрой коммуникации с клиентом, удобная оплата кредитных продуктов и сбор задолженности. Инструменты продвижения кроссмаркетинговых программ и система допродаж
		</p>
			
			<div class="row">
			
				<div class="col-md-6">
					
					<ul class="main-ul">
						<li>
						Полное соответствие требованиям 230 – ФЗ и законодательству о персональных данных

						
						</li>
					
						<li>
						Инструменты реструктуризации долга, пролонгации, реализация сценария обещанных платежей
	</li>
					
						<li>
						
					Интеграция платежного сервиса с возможностью совершения платежей онлайн
	</li>
					</ul>
					
				
				
				</div>
				
				<div class="col-md-6">
					<ul class="main-ul">
					
						<li>
						Легкий доступ клиента к данным личного кабинета информация о задолженности и возможности погашения

						
						</li>
						
						<li>
							Полноценная интеграция системы с АССЗ и АБС комплексами

						
						</li>
						
						<li>
							С2О с применением голосового сервиса (+IVR), меняет сценарий информирования в зависимости от действий заемщика

						
						</li>
						
					
					</ul>	
				</div>
				<div class="clear"></div>
				<hr>
				<div class="col-md-12">
				<p class="text-right blue-text"><a href="<?php Url::base(''); ?>/banki">
				
				Узнать подробнее 
				<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 62.1 116.09"><defs><style>.cls-1{fill:#0062ff;}</style></defs><title>Asset 1</title><g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1"><path class="cls-1" d="M1.2,1.19A4.1,4.1,0,0,0,1.2,7L52.3,58,1.2,109.09a4.1,4.1,0,1,0,5.8,5.8L60.9,61a4,4,0,0,0,1.2-2.9,4.18,4.18,0,0,0-1.2-2.9L7,1.29A4,4,0,0,0,1.2,1.19Z"/></g></g></svg>
				
				
				</a></p>
				</div>
			</div>
			
			
		</div>
	
	
	
	
	</div>
	
	
	
</div>	
</section>	
	
	