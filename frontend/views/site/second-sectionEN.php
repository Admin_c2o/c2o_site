
<?php 

use yii\helpers\Url;

?>



<section id="" class="second-sec">	
<div class="container">	
	
	
	<div class="row">
	
		<div class="col-md-6 img-box-wr">
			<img style="width: 75%;" src="images/2.png">
		</div>
	
	
		<div class="col-md-6">
			
			
			<p class="blue-text">Solution</p>
			<h2>Housing and utility enterprises </h2>
			
			<p class="lead">
		Effective distant tools for debt recovery, rent collection 
		</p>
			
			<div class="row">
			
				<div class="col-md-6">
					
					<ul class="main-ul">
						<li>Full compliance with local debt recovery and personal data laws
						</li>
					
						<li>
								Debt restructuring tools, rollovers, promised payments
						</li>
					
						<li>
						
							3 click online payments
						</li>
					</ul>
					
				
				
				</div>
				
				<div class="col-md-6">
					<ul class="main-ul">
					
						<li>
							No password access to the personal account with all info about debt and payment scenarios

						
						</li>
						
						<li>
								Multimedia interaction: banners, videos, photos, dynamic documents / pre-trial claims
						
						</li>
						
						<li>
								IVR + C2O changes the interaction scenario depending on borrower actions 

						
						</li>
						
					
					</ul>	
				</div>
				
				<div class="clear"></div>
				<hr>
				
				<div class="col-md-12">
				
				<p class="text-right blue-text"><a href="<?php Url::base(''); ?>/predpriyatiya-zhkkh">
				
				More...
				
				<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 62.1 116.09"><defs><style>.cls-1{fill:#0062ff;}</style></defs><title>Asset 1</title><g id="Layer_2" data-name="Layer 2"><g id="Layer_1-2" data-name="Layer 1"><path class="cls-1" d="M1.2,1.19A4.1,4.1,0,0,0,1.2,7L52.3,58,1.2,109.09a4.1,4.1,0,1,0,5.8,5.8L60.9,61a4,4,0,0,0,1.2-2.9,4.18,4.18,0,0,0-1.2-2.9L7,1.29A4,4,0,0,0,1.2,1.19Z"/></g></g></svg>
				
				</a></p>
				</div>
			</div>
			
			
		</div>
	
	
	
	
	</div>
	
	
	
</div>	
</section>	