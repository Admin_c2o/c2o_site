<?php

/* @var $this yii\web\View */


$this->title = 'Click to Offer - высокотехнологичная платформа для
	   коммуникации, информирования и сбора задолженностей с ваших клиентов';
?>



<div class="site-index">

<section id="" class="first-banner">


    <div class="container flex-center-align">

    <div class="jumbotron">
       

	   <h1>Кредитные организации. Банки.</h1>

	   <p class="lead">
	   
		Организация быстрой коммуникации с клиентами и сбор задолженности. Кроссмаркетинговые программы продвижения дополнительных услуг и сервисов.


		</p>

	   <img style="width: 35%;" class="" src="../images/bank_shapka.png">

	   </div>

	</div>
	
</section>	
	
	
	
<!-- ВТорая секция -->	
	
<section id="" class="second-sec">	
<div class="container">	
	
	
	<div class="row">
	<!--
		<div class="col-md-6">
			<img style="width: 75%;" src="images/2.png">
		</div>
	-->
	
		<div class="col-md-8">
			
			
			<p class="blue-text">Решение</p>
			<h2>Кредитные организации. Банки.
			</h2>
			
			<p class="lead">
			
			Организация быстрой коммуникации с клиентами и сбор задолженности. Кроссмаркетинговые программы продвижения дополнительных услуг и сервисов.

			
			</p>
		</div>
		
		
		<div class="col-md-4 img-box-wr">
			<img src="../images/bank_1.png">
			
				   
	   <br>
	   <br>
	   
	   	
	   
	   
		</div>
		
		
		
		
		<div class="col-md-12">
		
			<div class="row">
			
				<div class="col-md-4">
				
						
					<div class="clear"></div>
					<hr>
				
					
					<ul class="main-ul">
						<li>
						Полное соответствие требованиям Федерального закона 230-ФЗ, Федерального закона 152-ФЗ

						
						</li>
					
						<li>
							Инструменты пролонгации, реструктуризации, рассрочки выплаты по кредитным обязательствам


						</li>
					
						
					</ul>
					
					<div class="clear"></div>
				<hr>
				
				
				</div>
				
				<div class="col-md-4">
				
						
					<div class="clear"></div>
					<hr>
				
					<ul class="main-ul">
					
						<li>
						Интеграция любых платежных сервисов, включая Систему быстрых платежей ЦБ РФ

						
						</li>
						
						<li>
							Легкий доступ клиентов к информации о платежах/задолженности без запоминания номера кредитного договора и пароля
							
							
							<br>
							
							Эффективный сбор задолженности на этапе pre collection, soft collection
						
						</li>
						<!--
						<li>
							Голосовой сервис информирования потребителей с возможностью добавления сценариев взаимодействия 
						
						</li>
						-->
						
						
					
					</ul>	
					
					<div class="clear"></div>
				<hr>
					
				</div>
				
				
				
				<div class="col-md-4">
				
						
					<div class="clear"></div>
				<hr>
				
					<ul class="main-ul">
					
						<li>
						Интеграция с АССЗ и АБС комплексами. Установка платформы в контур.

						
						</li>
						
						<li>
						Голосовой сервис информирования клиентов с возможностью добавления сценариев взаимодействия 

						</li>
						
						
					
					</ul>	
					
					<div class="clear"></div>
				<hr>
					
				</div>
				
				
			
			</div>
			
			
		</div>
	
	
	<div class="clear"></div>
	
	<br>
	<div class="col-md-12">
	<h2>Оставьте заявку на Click to Offer <span  class="zayavka blue-zayavka">
			Заказать
		</span></h2> 
	</div>
	
	</div>
	
	
	
</div>	
</section>	
	
	
	

	
</div>
