<?php

/* @var $this yii\web\View */


$this->title = 'Click to Offer - высокотехнологичная платформа для
	   коммуникации, информирования и сбора задолженностей с ваших клиентов';
?>



<div class="site-index">

<section id="" class="first-banner">


    <div class="container flex-center-align">

    <div class="jumbotron">
       

	   <h1>Microfinance companies</h1>

	   <p class="lead">
	   Full control over debt recovery and a system for client informing with an approaching loan repayment date.
	   </p>


	   <img style="width: 35%;" class="" src="../images/5.png">


	   </div>

	
	</div>
	
</section>	
	
	
	
<!-- ВТорая секция -->	
	
<section id="" class="second-sec">	
<div class="container">	
	
	
	<div class="row">
	<!--
		<div class="col-md-6">
			<img style="width: 75%;" src="images/2.png">
		</div>
	-->
	
		<div class="col-md-8">
			
			
			<p class="blue-text">Solution</p>
			<h2>Microfinance companies</h2>
			
			<p class="lead">
			Full control over debt recovery and a system for client informing with an approaching loan repayment date.
			
			</p>
		
		</div>
		
		
		<div class="col-md-4 img-box-wr">
			<img src="../images/bank_1.png">
			
				   
	   <br>
	   <br>
	   
	   	
	   
	   
		</div>
		
		
		
		
		<div class="col-md-12">
		
			<div class="row">
			
				<div class="col-md-4">
				
						
					<div class="clear"></div>
					<hr>
				
					
					<ul class="main-ul">
						<li>
							Full compliance with local debt recovery and personal data laws
						</li>
					
						<li>
							Debt recovery tools, rollovers, promised payment scenarios
						</li>
					
						
					</ul>
					
					<div class="clear"></div>
				<hr>
				
				
				</div>
				
				<div class="col-md-4">
				
						
					<div class="clear"></div>
					<hr>
				
					<ul class="main-ul">
					
						<li>
						
								3 click online payment
						</li>
					
					
						<li>
							No password access to the personal account with all info about debt and payment scenarios
						
						</li>
						
						
					
					</ul>	
					
					<div class="clear"></div>
				<hr>
					
				</div>
				
				
				
				<div class="col-md-4">
				
						
					<div class="clear"></div>
				<hr>
				
					<ul class="main-ul">
					
						
						
						<li>
								Multimedia interaction: banners, videos, photos, dynamic documents / pre-trial claims
						
						</li>
						
						<li>
								IVR + C2O changes the interaction scenario depending on borrower actions 
						
						</li>
						
					
					</ul>	
					
					<div class="clear"></div>
				<hr>
					
				</div>
				
				
			
			</div>
			
			
		</div>
	
	
	<br>
	
	<div class="col-md-12">
	<h2>Make a request <span  class="zayavka blue-zayavka">
			Order
		</span></h2> 
	</div>
	
	</div>
	
	
	
</div>	
</section>	
	
	
	

	
</div>
